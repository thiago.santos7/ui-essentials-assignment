import "../css/styles.css";
import "../css/fontello.css";

import TaskUI from "./TaskUI.js";
import TaskStorage from "./TaskStorage.js";

const tasksEl = document.getElementById("tasks");

const ui = new TaskUI(tasksEl);

let storage;

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((response) => response.json())
  .then((json) => {
    storage = new TaskStorage(json);

    storage.getTasks().map((task) => addTask(task));
  })
  .catch(() => {
    storage = new TaskStorage([]);
  });

const idIterator = generateTaskID();

const submitButton = document.querySelector("#submit-button");

submitButton.addEventListener("click", addTaskEventHandler);

function* generateTaskID() {
  let i = storage.getTasks().length ?? 0;
  while (true) {
    yield i++;
  }
}

function addTask(newTask) {
  ui.addTask(newTask);
  storage.addTask(newTask);

  const lastEl = tasksEl.children[tasksEl.children.length - 1];

  lastEl
    .querySelector(`#delete-task-${newTask.id}`)
    .addEventListener("click", deleteTask);

  lastEl
    .querySelector(`#edit-task-${newTask.id}`)
    .addEventListener("click", editTask);
}

function addTaskEventHandler(e) {
  e.preventDefault();
  const inputEl = document.getElementsByTagName("input")[0];
  const taskName = inputEl.value;

  if (!taskName) {
    alert("Task cannot be empty!");
    return;
  }

  const newTask = {
    id: idIterator.next().value,
    name: taskName,
  };

  addTask(newTask);

  inputEl.value = "";
}

const getTaskElId = (e) => e.currentTarget.id.split("-")[2];

function editTask(e) {
  const taskID = getTaskElId(e);

  const inputEl = document.getElementsByTagName("input")[0];

  const task = storage.getTaskById(taskID);

  inputEl.value = task.name;

  submitButton.innerHTML = `
    <i class="icon-edit"></i>
    Save
  `;

  submitButton.removeEventListener("click", addTaskEventHandler);

  submitButton.addEventListener(
    "click",
    (e) => {
      e.preventDefault();

      task.name = inputEl.value;

      ui.editTask(task);
      storage.editTask(task);

      submitButton.innerHTML = `
        <i class="icon-plus-squared-alt"></i>
        Add
    `;

      submitButton.addEventListener("click", addTaskEventHandler);

      inputEl.value = "";
    },
    { once: true }
  );
}

function deleteTask(e) {
  const taskID = getTaskElId(e);

  storage.deleteTask(taskID);
  ui.deleteTask(taskID);
}
