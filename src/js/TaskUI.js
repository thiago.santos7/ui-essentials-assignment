class TaskUI {
  tasksParent;

  constructor(tasksParent) {
    this.tasksParent = tasksParent;
  }

  addTask(task) {
    const taskEl = document.createElement("article");
    const id = `task-${task.id}`;
    taskEl.id = id;
    taskEl.className =
      "flex justify-between my-3 border p-2 bg-gray-300 rounded";

    taskEl.innerHTML = `
              <section>
                  <h2 class="text-gray-700 font-bold">${task.name}</h2>
                  <p class="mt-6 text-gray-400 text-xs">Added ${new Date(
                    Date.now()
                  ).toLocaleString("pt-BR")}</p>
              </section>
              
              <section class="flex flex-col justify-between">
                  <button id="edit-${id}" class="hover:opacity-70">
                      <i class="icon-edit text-xl mb-2 text-blue-500"></i>
                  </button>
                  <button id="delete-${id}" class="hover:opacity-50">
                      <i class="icon-window-close-o text-xl text-red-500"></i>
                  </button>
              </section>
          `;

    this.tasksParent.appendChild(taskEl);
  }

  editTask(task) {
    const taskEl = this.tasksParent.querySelector(`#task-${task.id}`);

    taskEl.getElementsByTagName("h2")[0].innerHTML = task.name;
  }

  deleteTask(id) {
    const taskEl = this.tasksParent.querySelector(`#task-${id}`);
    this.tasksParent.removeChild(taskEl);
  }
}

export default TaskUI;
