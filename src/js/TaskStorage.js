class TaskStorage {
  tasks;

  constructor(tasks) {
    this.tasks = tasks;
  }

  addTask(task) {
    this.tasks.push(task);
  }

  editTask(targetTask) {
    this.tasks.some((task) => {
      if (task.id == targetTask.id) {
        task.title = targetTask.title;
        return true;
      }
      return false;
    });
  }

  deleteTask(id) {
    this.tasks = this.tasks.filter((task) => task.id != id);
  }

  getTasks() {
    return this.tasks;
  }

  getTaskById(id) {
    return this.tasks.filter((task) => task.id == id)[0];
  }
}

export default TaskStorage;
