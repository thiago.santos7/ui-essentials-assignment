# UI Essentials Assignment

This project is a simple To-do app. It's possible to add, update and remove tasks. Furthermore, an API is called to fill the list of task in the first time that the app is loaded, so the API call requirement filled.

## Tools

- Tailwindcss;
- PostCSS;
- Webpack.

## Command

First, install all dependencies:

```
npm install
```

Then you can execute in dev mode:

```
npm start
```

Or build for production:

```
npm run build
```

The build files will be created in the dist folder.
